const express = require('express');
const app = express();
const port = 3000;


app.use(express.static('public'));
app.set('view engine','ejs');

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/database_ourproject',{ useNewUrlParser: true }, (err) => {
  if (err) {
    console.log(err.message);
  }
  console.log('mongoose started');
});

app.get('/',(req, res) => {
  res.render('home');
});

app.get('/about',(req, res) => {
  res.render('about');
});

app.get('/contect',(req, res) => {
  res.render('contect');
});

app.get('/login',(req, res) => {
  res.render('login');
});

app.get('/signup',(req, res) => {
  res.render('signup');
});





app.listen(port, () =>{
    console.log(`server started on port ${port}`)
});